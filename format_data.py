#!/usr/bin/python3

import pandas as pd


def main():
    """
    This program reformats the dates of the csv file into two heads: Summer 2018 and Winter 2018.
    It also reformats the different crime categories into two: Burglaries and Robberies.
    Thereafter it prints a 2x2 contingency table.
    """
    df = pd.read_csv('cleaned_data.csv', sep=';', skiprows=[1])
    df['Summer 2018'] = (df['2018 juni'] + df['2018 juli']
                         + df['2018 augustus'])
    df['Winter 2018'] = (df['2018 januari '] + df['2018 februari']
                         + df['2018 december'])
    df = df.drop(['2018 januari ', '2018 februari', '2018 juni', '2018 juli',
                  '2018 augustus', '2018 december'], axis=1)
    df = df.replace(['1.1.1 Diefstal/inbraak woning',
                     '1.1.2 Diefstal/inbraak box/garage/schuur',
                     '2.5.1 Diefstal/inbraak bedrijven enz.'], 'Burglaries')
    df = df.replace(['1.4.6 Straatroof', '1.4.7 Overval'], 'Robberies')
    df = df.rename(columns={'Unnamed: 0': 'Crime'})
    group_functions = {'Summer 2018': 'sum', 'Winter 2018': 'sum'}
    df = df.groupby(df['Crime']).aggregate(group_functions)
    print(df)


if __name__ == '__main__':
    main()
