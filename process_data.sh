#!/bin/bash

# Usage: bash process_data,sh or ./process_data.sh


cat *.csv | \
grep -E '.*;.*;.*;.*' > cleaned_data.csv

python3 format_data.py

rm -f cleaned_data.csv
